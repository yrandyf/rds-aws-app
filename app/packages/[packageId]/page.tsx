import React from "react";
import Link from "next/link";

const page = ({ params }) => {
  const tourPackages = [
    {
      id: 1,
      packageName: "Explore Sri Lanka",
      description:
        "Discover the rich cultural heritage, stunning landscapes, and vibrant cities of Sri Lanka on this 10-day adventure.",
      duration: "10 days",
    },
    {
      id: 2,
      packageName: "Sri Lankan Wildlife Safari",
      description:
        "Embark on a 7-day wildlife safari through Sri Lanka's national parks and nature reserves to witness diverse wildlife.",
      duration: "7 days",
    },
    {
      id: 3,
      packageName: "Beach Paradise Getaway",
      description:
        "Relax and unwind on Sri Lanka's pristine beaches during this 14-day beach paradise getaway.",
      duration: "14 days",
    },
    {
      id: 4,
      packageName: "Cultural Heritage Tour",
      description:
        "Immerse yourself in Sri Lanka's rich history and architecture with this 8-day cultural heritage tour.",
      duration: "8 days",
    },
  ];

  const selectedTour = tourPackages.find((tour) => tour.id == params.packageId);
  console.log("selected", selectedTour, params.packageId);
  return (
    <div className="text-center">
      <ul className="list-disc">
        <li className="text-blue-500">
          <h3 className="font-bold">{selectedTour?.packageName}</h3>
          <p>{selectedTour?.description}</p>
          <p className="text-gray-500">{selectedTour?.duration}</p>
        </li>
      </ul>
      <Link href={`/packages/`}>
        <h3 className="font-bold">Go Back</h3>
      </Link>
    </div>
  );
};

export default page;

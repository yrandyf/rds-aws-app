import React from "react";
import Link from 'next/link';

const page = () => {
  const tourPackages = [
    {
        id : 1,
        packageName: "Explore Sri Lanka",
        description: "Discover the rich cultural heritage, stunning landscapes, and vibrant cities of Sri Lanka on this 10-day adventure.",
        duration: "10 days"
    },
    {
        id : 2,
        packageName: "Sri Lankan Wildlife Safari",
        description: "Embark on a 7-day wildlife safari through Sri Lanka's national parks and nature reserves to witness diverse wildlife.",
        duration: "7 days"
    },
    {
        id : 3,
        packageName: "Beach Paradise Getaway",
        description: "Relax and unwind on Sri Lanka's pristine beaches during this 14-day beach paradise getaway.",
        duration: "14 days"
    },
    {
        id : 4,
        packageName: "Cultural Heritage Tour",
        description: "Immerse yourself in Sri Lanka's rich history and architecture with this 8-day cultural heritage tour.",
        duration: "8 days"
    }
  ];

  return (
    <div className="text-center">
      <ul className="list-disc">
        {tourPackages.map((tour, index) => (
          <li key={index} className="text-blue-500">
            <Link href={`/packages/${encodeURIComponent(tour.id)}`}>
              <h3 className="font-bold">{tour.packageName}</h3>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default page;
